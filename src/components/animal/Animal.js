import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import InputText from "./input/InputText";
import Output from "./output/Output";

class Aninmal extends Component {
    constructor(props){
        super(props);
        this.state = {
            kind: "",
            outputMessage: false,
            imgCat: false
        }
    }
    checkKind = (value) => {
        this.setState({
            kind: value
        })
    }
    outputResult = () => {
        if(this.state.kind.toLocaleUpperCase().trim() === "CAT") {
            this.setState({
                outputMessage: false,
                imgCat: true
            })
        }
        else {
            this.setState({
                outputMessage: true,
                imgCat: false
            })
        }
    }
    render() {
        return (
            <>
                <div className="container text-center mt-5">
                    <InputText kindProp={this.state.kind} checkKindProp={this.checkKind} outputResultProp={this.outputResult}/>
                    <Output outputMessageProp={this.state.outputMessage} imgCatProp={this.state.imgCat}/>
                </div>
            </>
        )
    }
}
export default Aninmal;