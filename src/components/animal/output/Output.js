import { Component } from "react";
import imageCat from '../../../assets/images/cat.jpg';

class Output extends Component {
    render() {
        const { imgCatProp, outputMessageProp } = this.props;
        return (
            <>
                <div className="row mt-4">
                    <div className="col">
                        {imgCatProp ? <img src={imageCat} alt="cat" /> : null}

                    </div>
                </div>
                <div className="row mt-4">
                    {outputMessageProp ? <p>meow not found :&#41;</p> : null}
                </div>
            </>
        )
    }
}
export default Output;