import { Component } from "react";

class InputText extends Component {
    onBtnClickHandler = () => {
        const { outputResultProp } = this.props;
        outputResultProp();
    }

    inputChangeValue = (event) => {
        const { checkKindProp } = this.props;
        checkKindProp(event.target.value);
    }
    render() {
        const { kindProp } = this.props;
        return (
            <>
                <div className="row">
                    <div className="col">
                        <input placeholder="Tìm kiếm" onChange={this.inputChangeValue} value={kindProp}></input>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col">
                        <button className="button" onClick={this.onBtnClickHandler}>Search</button>
                    </div>
                </div>
            </>


        )
    }
}
export default InputText;